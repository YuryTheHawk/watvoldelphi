unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit0: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Button1: TButton;
    Image1: TImage;
    Button2: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Button3: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Edit0KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit0KeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  Sky, Water, Land: TBitmap;

implementation

{$R *.dfm}

procedure LoadSprite(var B: TBitmap; s: string);
begin
 B:=TBitmap.Create;
 B.LoadFromFile(s);
 B.Width:=24;
 B.Height:=24;
end;

procedure TForm1.FormCreate(Sender: TObject);
var i, j: byte;
begin
 Randomize;
 LoadSprite(Sky,'txd/sky.bmp');
 LoadSprite(Water,'txd/water.bmp');
 LoadSprite(Land,'txd/land.bmp');
 for i:=0 to 8 do for j:=0 to 9 do Image1.Canvas.Draw(i*24,j*24,Sky);
end;

procedure TForm1.Edit0KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var p: ^TEdit;
    c: char;
begin
 p:=@Sender;
 c:=p^.Text[1];
 case Key of
  VK_DOWN: if c='9' then p^.Text:='0' else p^.Text:=IntToStr(StrToInt(c)+1)[1];
  VK_UP:   if c='0' then p^.Text:='9' else p^.Text:=IntToStr(StrToInt(c)-1)[1];
  48..57:  p^.Text:=Chr(Key);
 end;
 Key:=0;
end;

procedure TForm1.Edit0KeyPress(Sender: TObject; var Key: Char);
begin
 Key:=#0;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
 Edit0.Text:=IntToStr(random(10));
 Edit1.Text:=IntToStr(random(10));
 Edit2.Text:=IntToStr(random(10));
 Edit3.Text:=IntToStr(random(10));
 Edit4.Text:=IntToStr(random(10));
 Edit5.Text:=IntToStr(random(10));
 Edit6.Text:=IntToStr(random(10));
 Edit7.Text:=IntToStr(random(10));
 Edit8.Text:=IntToStr(random(10));
end;

procedure TForm1.Button2Click(Sender: TObject);
var q, w: array[0..8] of integer;
    p: array[0..8,0..8] of integer;
    i, j, a0, vol, a, b, amax, bmax: integer;
begin
 q[0]:=StrToInt(Edit0.Text);
 q[1]:=StrToInt(Edit1.Text);
 q[2]:=StrToInt(Edit2.Text);
 q[3]:=StrToInt(Edit3.Text);
 q[4]:=StrToInt(Edit4.Text);
 q[5]:=StrToInt(Edit5.Text);
 q[6]:=StrToInt(Edit6.Text);
 q[7]:=StrToInt(Edit7.Text);
 q[8]:=StrToInt(Edit8.Text);
 // ��������� �������, ��������� ������ ���� � ������ �������.
 // �� ���������� ��� ��������� ���� � �������
 for i:=0 to 8 do w[i]:=0;
 a0:=0;
 vol:=0;
 for i:=0 to 8 do begin
  // ���������� ������ ������ �������, �����������
  // ������ ������� � �������� ���������
  if (q[i]=0) or (i=8) then begin
   a:=a0;
   b:=i;
   // � ������ ������� ��������� ������ � ������� ������
   // ������������� ������� ����������
   amax:=0;
   bmax:=0;
   while (a<=b) do begin
    if (q[a]>amax) then amax:=q[a];
    if (q[b]>bmax) then bmax:=q[b];
    // ��� ������� "����������" ����� �����������
    // ����� ������������� �� ������� ������� � ���������
    if (amax>=bmax) then begin
     w[b]:=bmax-q[b];
     vol:=vol+w[b];
     b:=b-1;
    end else begin
     w[a]:=amax-q[a];
     vol:=vol+w[a];
     a:=a+1;
    end;
   end;
   // ��� �������� � ��������� ������� �������� �� ������ �������
   a0:=i+1;
  end;
 end;
 // ������������ ����� ������� ��� ������ �� �����
 for i:=0 to 8 do
  for j:=0 to 8 do
   if (j>8-q[i]) then p[i,j]:=1
    else if (j>8-q[i]-w[i]) then p[i,j]:=2 else p[i,j]:=0;
 // ����� �� ����� �������, ������������ ��������� �������
 for j:=0 to 8 do
  for i:=0 to 8 do
   case p[i,j] of
    0: Image1.Canvas.Draw(i*24,j*24,Sky);
    1: Image1.Canvas.Draw(i*24,j*24,Land);
    2: Image1.Canvas.Draw(i*24,j*24,Water);
   end;
 Label1.Caption:='V = '+IntToStr(vol);
 Label2.Caption:='S = (';
 for i:=0 to 8 do begin
  Label2.Caption:=Label2.Caption+IntToStr(q[i]);
  if (i=8) then Label2.Caption:=Label2.Caption+')'
           else Label2.Caption:=Label2.Caption+',';
 end;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
 ShowMessage('�������� ������������������ �� 9 ����, �� 0 �� 9.'#13+
 '������ ����� ��������� ������ �����. ���� ������ �� ��� ����������� ����,'#13+
 '�� ��������� ���������� ���� ���������� ����� ��������.'#13+
 '��������� 1�1 - �������� � ���� ����� ����, ������ 1 �����.'#13#13+
 '��������� ������������ ����������� ����� �������� ����� ����.');
end;

end.
