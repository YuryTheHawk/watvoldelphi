object Form1: TForm1
  Left = 489
  Top = 313
  Width = 300
  Height = 420
  BorderIcons = [biSystemMenu]
  Caption = 'D1 in Delphi'
  Color = clBtnFace
  Constraints.MaxHeight = 420
  Constraints.MaxWidth = 300
  Constraints.MinHeight = 420
  Constraints.MinWidth = 300
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -19
  Font.Name = '@Arial Unicode MS'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 25
  object Image1: TImage
    Left = 31
    Top = 64
    Width = 216
    Height = 216
  end
  object Label1: TLabel
    Left = 15
    Top = 8
    Width = 45
    Height = 25
    Caption = 'V = 0'
  end
  object Label2: TLabel
    Left = 15
    Top = 32
    Width = 249
    Height = 25
    Caption = 'S = (0, 0, 0, 0, 0, 0, 0, 0, 0)'
  end
  object Edit1: TEdit
    Left = 56
    Top = 288
    Width = 20
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = '@Arial Unicode MS'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Text = '0'
    OnKeyDown = Edit0KeyDown
    OnKeyPress = Edit0KeyPress
  end
  object Edit2: TEdit
    Left = 80
    Top = 288
    Width = 20
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = '@Arial Unicode MS'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Text = '0'
    OnKeyDown = Edit0KeyDown
    OnKeyPress = Edit0KeyPress
  end
  object Edit3: TEdit
    Left = 104
    Top = 288
    Width = 20
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = '@Arial Unicode MS'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Text = '0'
    OnKeyDown = Edit0KeyDown
    OnKeyPress = Edit0KeyPress
  end
  object Edit0: TEdit
    Left = 32
    Top = 288
    Width = 20
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = '@Arial Unicode MS'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Text = '0'
    OnKeyDown = Edit0KeyDown
    OnKeyPress = Edit0KeyPress
  end
  object Edit4: TEdit
    Left = 128
    Top = 288
    Width = 20
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = '@Arial Unicode MS'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Text = '0'
    OnKeyDown = Edit0KeyDown
    OnKeyPress = Edit0KeyPress
  end
  object Edit5: TEdit
    Left = 152
    Top = 288
    Width = 20
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = '@Arial Unicode MS'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Text = '0'
    OnKeyDown = Edit0KeyDown
    OnKeyPress = Edit0KeyPress
  end
  object Edit6: TEdit
    Left = 176
    Top = 288
    Width = 20
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = '@Arial Unicode MS'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    Text = '0'
    OnKeyDown = Edit0KeyDown
    OnKeyPress = Edit0KeyPress
  end
  object Edit7: TEdit
    Left = 200
    Top = 288
    Width = 20
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = '@Arial Unicode MS'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    Text = '0'
    OnKeyDown = Edit0KeyDown
    OnKeyPress = Edit0KeyPress
  end
  object Edit8: TEdit
    Left = 224
    Top = 288
    Width = 20
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = '@Arial Unicode MS'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    Text = '0'
    OnKeyDown = Edit0KeyDown
    OnKeyPress = Edit0KeyPress
  end
  object Button1: TButton
    Left = 13
    Top = 328
    Width = 97
    Height = 33
    Caption = #1057#1083#1091#1095#1072#1081#1085#1086
    TabOrder = 9
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 117
    Top = 328
    Width = 113
    Height = 33
    Caption = #1056#1072#1089#1089#1095#1080#1090#1072#1090#1100
    TabOrder = 10
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 237
    Top = 328
    Width = 25
    Height = 33
    Caption = '?'
    TabOrder = 11
    OnClick = Button3Click
  end
end
